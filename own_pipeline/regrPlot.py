import seaborn as sns
import pandas as pd


gene_id_dict = {}
with open('/groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output/ProbeAnnotation-gencode.v32.primary_assembly-EnsgToGenes.txt') as f:
    for line in f:
        key, val = line.strip().split('\t')
        gene_id_dict[key] = val
f.close()


def make_plot(splice_id, gene_id, splice_type, gene_name=None):
    x_file = '/groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output/validationRsQTL/regr_plot_{}_x.txt'.format(gene_id)
    y_file = '/groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output/validationRsQTL/regr_plot_{}_y.txt'.format(gene_id)

    x = pd.read_csv(x_file, sep='\t')
    y = pd.read_csv(y_file, sep='\t')

    xandy = pd.concat([y, x], axis=1)
    print(xandy)
    if splice_type == 'MXE':
        graph = sns.regplot(x="Genotype",
                          y=splice_id,
                          data=xandy, logistic=False)
    else:
        graph = sns.regplot(x="Genotype",
                          y=splice_id,
                          data=xandy, logistic=True)

    if gene_name:
        gene_name = gene_id_dict.get(gene_name)
        print(gene_name)
        graph.set(ylabel=gene_name)
    else:
        graph.set(ylabel=gene_id)

    savename = '/groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output/validationRsQTL/regressionPlots/{}_{}_regressionPlot.png'.format(splice_type, gene_id)
    graph.figure.savefig(savename)
    graph.figure.clf()


make_plot('ENSG00000160221.18|44140941_44141417_44141163_44141417_44140249_44140342|ENSE00001613924.2|ENSE00003474302.1,ENSE00003626079.1|ENSE00003482790.1,ENSE00003665668.1',
          'ENSG00000160221', 'A3SS', gene_name='ENSG00000160221.18')
make_plot('ENSG00000176731.12|85219235_85219363_85219322_85219363_85217385_85217502|ENSE00003751264.1|ENSE00001406568.1|ENSE00001262841.2',
          'ENSG00000176731', 'A5SS', gene_name='ENSG00000176731.12')
make_plot('ENSG00000100744.15|96379687_96379788_96379687_96379755_96382246_96382505|ENSE00002520484.1|ENSE00002450067.1|ENSE00001175804.1',
          'ENSG00000100744', 'A5SS',gene_name='ENSG00000100744.15')
make_plot('ENSG00000187010.21|25307646_25307816_25316999_25317079_25306595_25306729|ENSE00001463271.1|ENSE00003466840.1,ENSE00003523667.1,ENSE00003680748.1|ENSE00001333242.1',
          'ENSG00000187010', 'MXE',gene_name='ENSG00000187010.21')
make_plot('ENSG00000214087.8|81682033_81683594_81682033_81682675_81683535_81683594|ENSE00002659945.1|ENSE00002662291.1|ENSE00003561426.1,ENSE00003644564.1',
          'ENSG00000214087', 'RI',gene_name='ENSG00000214087.8')

y_file = '/groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output/2020-05-26-Cortex-EUR-CovariatesRemovedOLS-ENSG00000100744.txt'
y = pd.read_csv(y_file, sep='\t', index_col=0)
# x = x.set_index('-')
y = y.transpose()


x_file = '/groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output/validationRsQTL/regr_plot_ENSG00000100744_x.txt'
x = pd.read_csv(x_file, sep='\t', index_col=0)
x['Genotype'] = x['Genotype'].round()

xandy = pd.concat([y, x], axis=1)
sns.set_theme(style="whitegrid")
ax = sns.boxplot(x="Genotype", y="ENSG00000100744.15", data=xandy)
ax.figure.savefig('/groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output/validationRsQTL/regressionPlots/boxplot_ENSG00000100744.png')

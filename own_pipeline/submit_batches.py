#!/usr/bin/env python
import glob
import argparse
import os
import time

parser = argparse.ArgumentParser(description="Divide cramfiles in seperate batches")
parser.add_argument("cramfile_dir", help="Location of the input cramfiles")
parser.add_argument("job_dir", help="Directory to write jobs to")
parser.add_argument("output_dir", help="Output directory for rMats results")
parser.add_argument("batch_size", help="Size of the batches to create")
parser.add_argument("study", help="study of the cramfiles")

# Fetch command line arguments
args = parser.parse_args()
input_dir = args.cramfile_dir
output_dir = args.output_dir
job_dir = args.job_dir

annotation_dir = "/apps/data/ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_32/gencode.v32.primary_assembly.annotation.gtf"
libblas = "/groups/umcg-biogen/tmp04/tools/brain_eQTL/utility_scripts/job_generating_scripts/usr/lib64"
tmp_loc = " /groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output/temp-cram/"

# Fetch the cramfiles and divide in blocks of 20
cramfiles = glob.glob(input_dir + "*.cram")
batch_size = int(args.batch_size)
crambatches = []
last = 0.0

while last < len(cramfiles):
    crambatches.append(cramfiles[int(last):int(last + batch_size)])
    last += batch_size

# Analyse the cram file in separate batches
for i in range(len(crambatches)):
    print("Copying batch ", i + 1)
    exp_samples = []
    batch_samples = []
    for j in range(len(crambatches[i])):
        # Fetch samplename from batch
        sample = crambatches[i][j].split('/')[-1].split(".cram")[0]
        batch_samples.append(sample)
        exp_samples.append(sample + ".done")

        # Copy cram file to tmp folder
        os.system("cp " + crambatches[i][j] + tmp_loc)

    # run generate_rMats_jobs.py
    print("Creating rMats jobs...")
    gen_jobs_args = "python /groups/umcg-biogen/tmp04/output/2019-11-08-FreezeTwoDotOne/2020-09-16-rMats-output" \
                    "/scripts/generate_rMats_jobs.py {} {} {} {} {} {}".format(output_dir, job_dir, libblas,
                                                                               annotation_dir, tmp_loc, args.study)
    os.system(gen_jobs_args)
    # Wait a bit for the shell scripts to be made
    time.sleep(5)

    # submit .sh files to cluster
    print("Submitting jobs to cluster...")
    for sh in glob.glob(job_dir + args.study + "/*.sh"):
        if sh.split('/')[-1].split(".sh")[0] in batch_samples:
            com = "sbatch " + sh
            os.popen(com, "r", 1)

    # Wait for each sample.done file
    print("Waiting for the results...")
    check = False
    while not check:
        time.sleep(60)
        res_samples = []
        for file in glob.glob(output_dir + args.study + "/*.done"):
            res_samples.append(os.path.basename(file))
        check = all(item in res_samples for item in exp_samples)

    print("Got all results from this batch, now deleting cram files...")
    # Remove cram files when results are in
    os.system("rm " + tmp_loc + "*.cram")

#!/usr/bin/env python
import numpy as np
import pandas as pd
import argparse
import glob
import statsmodels.api as sm
import difflib
import os
import collections

parser = argparse.ArgumentParser("perform linear regression on data")
parser.add_argument("input_dir", help="Directory with input PSI files")
parser.add_argument("out_dir", help="Directory to write results to")
parser.add_argument("sample", help="Sample annotation files ")
parser.add_argument("cov_file", help="Covariates file")
parser.add_argument("--conversion", help="file with conversion dictionary for the sample names")

args = parser.parse_args()
input_dir = args.input_dir
out_dir = args.out_dir
sample_file = args.sample
cov_file = args.cov_file

psi_files = glob.glob(input_dir + '/*Genelevel_ZscoreFiltered.txt')


def removeX(name):
    if name.startswith("X"):
        name = name[1:]
    return name


# Read in the covariates file
cov = pd.read_csv(cov_file, sep="\t", header=0)


conversion_dict = {}
with open(args.conversion) as f:
    for line in f:
        if line.strip() == "sampleCovar":
            continue
        key, val = line.strip("\n").split("\t")
        if key[0].isdigit():
            key = 'X' + key
        conversion_dict[key] = val

splice_types = ["A3SS", "A5SS", "MXE", "RI", "SE"]
for file in psi_files:
    print(file)
    splicetype = file.split("/")[-1].split('_')[0]
    print(splicetype)
    print(input_dir + splicetype + 'sampleAnnotationFull.txt')

    sample_annotation = pd.read_csv(input_dir + splicetype + '_sampleAnnotationFull.txt', sep="\t", header=0)
    out_file = open(out_dir + splicetype + '_covarCorrectedResidLogit.txt', "a")
    out_file_OLS = open(out_dir + splicetype + '_covarCorrectedResidOLS.txt', "a")

    df = pd.read_csv(file, sep="\t", header=0)
    matches = []

    covar_removed = ['PCT_CODING_BASES', 'PF_HQ_ALIGNED_READS', 'PF_HQ_ALIGNED_Q20_BASES', 'PF_READS_ALIGNED',
                     'avg_mapped_read_length', 'total_reads']

    for sample_header in df.columns:
        out_file.write(sample_header + "\t")
        out_file_OLS.write(sample_header + "\t")
    out_file.write("\n")
    out_file_OLS.write("\n")

    print(df)

    zero_var_counter = 0
    rsquared_spl_event = {}
    print(df.columns.values)
    sample_cov = cov.loc[cov["-"].isin(df.columns.values)].set_index('-')
    sample_cov = sample_cov.drop(covar_removed, axis=1)

    null_data = sample_cov[sample_cov.isnull().any(axis=1)]
    sample_cov = sample_cov.drop(list(null_data.index))

    for index, splice_event in df.iterrows():
        x = sample_cov
        # Loading the imputed psi values
        y = pd.DataFrame(splice_event)
        spl_gene = y.columns[0]

        x = x.reindex(y.index)

        null_data = x[x.isnull().any(axis=1)]
        x = x.drop(list(null_data.index))
        y = y.drop(list(null_data.index))

        y_bin = y.round()

        try:
            logit_results = sm.Logit(y_bin, x).fit()
            OLS_results = sm.OLS(y, x).fit()
            OLS_results_bin = sm.OLS(y_bin, x).fit()
            OLS_resid = pd.DataFrame(OLS_results_bin.resid, columns=[spl_gene])
            OLS_resid_list = OLS_resid.iloc[:, 0].tolist()

            resid = pd.DataFrame(logit_results.resid_generalized, columns=[spl_gene])
            resid_list = resid.iloc[:, 0].tolist()

            out_file.write(spl_gene + "\t")
            for val in resid_list:
                out_file.write(str(val) + "\t")
            out_file.write("\n")

            out_file_OLS.write(spl_gene + "\t")
            for val in OLS_resid_list:
                out_file_OLS.write(str(val) + "\t")
            out_file_OLS.write("\n")

            rsquared_spl_event[index] = [logit_results.prsquared, OLS_results.rsquared, OLS_results_bin.rsquared]
        except :
            zero_var_counter += 1
            continue
    print(zero_var_counter)
    print("Writing rsquared data to files....")

    splice_rsqrd = pd.DataFrame.from_dict(rsquared_spl_event, orient='index', columns=['R2.Logit',
                                                                                       'R2.OLS', 'R2.OLS_bin'])
    splice_rsqrd.to_csv(out_dir + splicetype + '_RsquaredPerSpliceEvent.txt', sep="\t")
    out_file.close()
    break



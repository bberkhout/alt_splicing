#!/usr/bin/env python
import pandas as pd
import argparse
import statsmodels.api as sm
from statsmodels.genmod import families
from scipy import stats
import warnings


parser = argparse.ArgumentParser("perform sQTL analysis")
parser.add_argument("input_dir", help="Directory with input PSI files")
parser.add_argument("out_dir", help="Directory to write results to")
parser.add_argument("cov_file", help="Covariates file")
parser.add_argument('genotype_data', help='file containing genotype data')
parser.add_argument('sample_links', help='file linking genotype sample names with expression names')
parser.add_argument('genes_links', help='file containing gene ensemble IDs and SNP locs')

args = parser.parse_args()
input_dir = args.input_dir
out_dir = args.out_dir
cov_file = args.cov_file
genotype_file = args.genotype_data

warnings.filterwarnings("ignore")
# Read covariate file
cov = pd.read_csv(cov_file, sep="\t", header=0)

# Read genotype data
genotype_df = pd.read_csv(genotype_file, sep='\t', header=0)


def make_dict_from_file(file, reverse=False):
    dict = {}
    with open(file) as f:
        for line in f:
            if reverse:
                val, key = line.strip().split('\t')
            else:
                key, val = line.strip().split('\t')
            dict[key] = val
    f.close()
    return dict


def check_vif(covariates):
    count = 1
    covar_removed = []
    for i in range(covariates.shape[1]):
        if count == covariates.shape[1]:
            break
        count = 1

        for covar in covariates:
            if covar != 'Genotype':
                y_cov = covariates[covar]
                x_cov = covariates.drop(covar, axis=1)

                res = sm.OLS(y_cov.astype(float), x_cov.astype(float)).fit()
                rsquared = res.rsquared
                if rsquared > 0.999:
                    covariates = covariates.drop(covar, axis=1)
                    covar_removed.append(covar)
                    break
                else:
                    count += 1

    return covar_removed


def zToP(z):
    if z > 0.0:
        p = stats.norm.sf(-z)
    else:
        p = stats.norm.sf(z)

    if p > 0.5:
        p = 1.0 - p
    p *= 2.0
    return p


conversion_dict = make_dict_from_file(args.sample_links)
gene_dict = make_dict_from_file(args.genes_links, reverse=True)

genotype_names = list(genotype_df.columns)
converted_samples = [conversion_dict.get(item, item) for item in genotype_names[3:]]
genotype_names[3:] = converted_samples
genotype_df.columns = genotype_names
genotype_df = genotype_df.set_index('SNP')

cov = cov.loc[:, (cov != 0).any(axis=0)]
splice_types = ['A3SS', 'A5SS', 'MXE', "RI", "SE"]

for spl_type in splice_types:
    print('Finding sQTLs for ', spl_type)
    genotype_df_adj = genotype_df
    overlap_counter = 0
    none_dound_counter = 0
    sqtl_counter = 0
    sign_counter = 0
    bugged_count = 0
    sum_count = 0

    # Create output file
    out_file = open(out_dir + spl_type + '_sQTLs.txt', "w")
    not_tested = open(out_dir + spl_type + '_eventsNotTested.txt', 'w')
    # # Write header to file
    header = ['spliceEventID', 'SNP', 'allele', 'minorAllele', 'gene', 'n', 'pval',
              'beta', 's.e', 'beta/s.e', 'sumY']
    for i in header:
        out_file.write(i + '\t')
    out_file.write('\n')

    spl_data = pd.read_csv(input_dir + spl_type + '_Genelevel_ZscoreFiltered.txt', sep='\t', header=0)

    sample_cov = cov.loc[cov["Sample"].isin(spl_data.columns.values)].set_index("Sample")
    counter = 0

    # Remove emptpy data from the covariates table
    null_data = sample_cov[sample_cov.isnull().any(axis=1)]
    sample_cov = sample_cov.drop(list(null_data.index))
    sample_cov = sample_cov.loc[:, (sample_cov != 0).any(axis=0)]

    for index, splice_event in spl_data.iterrows():
        x = sample_cov

        # Loading the psi values
        y = pd.DataFrame(splice_event)
        spl_event_id = y.columns[0]

        spl_gene = y.columns[0].split('|')[0]
        sum_count += 1
        try:
            # print(spl_gene)
            # Get SNP corresponding to the splice event
            snpID = gene_dict[spl_gene]
            allele = genotype_df_adj.loc[snpID][0]
            minor_allele = genotype_df_adj.loc[snpID][1]
            genotype_data = genotype_df_adj.loc[snpID][2:]

            # Get the sample that appear in both covariate and expression data
            genotype_data = genotype_data.loc[genotype_data.index.isin(x.index)]
            genotype_data = genotype_data.loc[genotype_data.index.isin(y.index)]

            # Remove samples with no genotype data available
            non_negative_genotypes = genotype_data.loc[genotype_data != -1]
            non_negative_genotypes = non_negative_genotypes.dropna()
            x['Genotype'] = non_negative_genotypes.iloc[2:]

            x = x.reindex(non_negative_genotypes.index)
            y = y.reindex(non_negative_genotypes.index)

            # Remove columns filled with only 0
            x = x.loc[:, (x != 0).any(axis=0)]

            null_data = x[x.isnull().any(axis=1)]
            x = x.drop(list(null_data.index))
            y = y.drop(list(null_data.index))

            # Check if the distribution of 1 and 0 for y is valid (not perfectly separable)
            occurances = y.value_counts().tolist()

            if len(occurances) == 2:
                zero_occ = occurances[0]
                one_occ = occurances[1]
                if zero_occ < 20 or one_occ < 20:
                    bugged_count += 1
                    continue
            else:
                bugged_count += 1
                continue

            covar_removed = check_vif(x)

            x = x.drop(covar_removed, axis=1)
            x = sm.add_constant(x)

            y = y.reindex(x.index)

            # Convert data to float
            x = x.astype(float)
            y = y.astype(float)

            # Second logistic regression with the genotype data added
            if spl_type == "MXE":
                linreg_results = sm.GLM(y, x).fit()
                std_err = linreg_results.bse.values[-1]
                beta = linreg_results.params.values[-1]
                dev1 = linreg_results.deviance

                z_score = beta / std_err
                pval = zToP(z_score)

            else:
                y = y.round()

                logit_results_genotype = sm.GLM(y, x, family=families.Binomial()).fit()

                dev1 = logit_results_genotype.deviance
                std_err = logit_results_genotype.bse.values[-1]
                beta = logit_results_genotype.params.values[-1]

                z_score = beta/std_err
                pval = zToP(z_score)

            if dev1 != dev1:  # Check if model results are viable
                bugged_count += 1
                not_tested.write(spl_event_id + '\n')
                continue

            out_data = [spl_event_id, snpID, allele, minor_allele, spl_gene, len(y.index),
                        pval, beta, std_err, beta/std_err, y.sum()[0]]
            for i in out_data:
                out_file.write(str(i) + '\t')
            out_file.write('\n')

            if pval < 0.05:
                sign_counter += 1
            sqtl_counter += 1
        except KeyError:
            none_dound_counter += 1
            continue
        print('progress: ', sum_count, 'total: ', spl_data.shape[0], ' sQTLs tested: ', sqtl_counter, end='\r')
    print('\n')
    print("# of sQTLs found: ", sqtl_counter)
    print('genes with no cis eQTL: ', none_dound_counter)
    print('significant sQTLs found: ', sign_counter)
    print('Genes not tested: ', bugged_count)
    print('total splice events: ', sum_count)
    out_file.close()

#!/usr/bin/env python
import numpy as np
import matplotlib as plt
import seaborn as sns
import pandas as pd
import argparse
import glob
import statsmodels.api as sm
import difflib

parser = argparse.ArgumentParser("perform linear regression on data")
parser.add_argument("sample", help="Sample annotation file")
parser.add_argument("cov_file", help="Covariates file")

args = parser.parse_args()
sample_file = args.sample
cov_file = args.cov_file

# Read in the covariates file
cov = pd.read_csv(cov_file, sep="\t", header=0)
sample_annotation = pd.read_csv(sample_file, sep="\t", header=0, decimal='.')

sample_cov = cov.loc[cov["Sample"].isin(sample_annotation["rnaseq_id"])].set_index('Sample')
null_data = sample_cov[sample_cov.isnull().any(axis=1)]
sample_cov = sample_cov.drop(list(null_data.index))

heat_map = sns.heatmap(sample_cov.corr())
heat_map.figure.savefig('covariateHeatmmap.pdf',  bbox_inches="tight")
covar_removed = []
for i in range(20):
    print("starting loop ", i)
    for covar in sample_cov:
        y = sample_cov[covar]
        x = sample_cov.drop(covar, axis=1)

        res = sm.OLS(y, x).fit()
        print(res.rsquared)
        rsquared = res.rsquared
        vif = 1/(1-rsquared)
        if rsquared > 0.999:
            sample_cov = sample_cov.drop(covar, axis=1)
            covar_removed.append(covar)
            print("Removed: ", covar, '\n')
            break

print('removed following covars: ', covar_removed)



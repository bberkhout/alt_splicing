# Alternative splicing in the human brain

This project is made to identify sQTLs in the human brain.  
## Requierments and packages  
***R*** Version 3.5.1  
***Python*** Version 3.6.3  

### Python Packages
***Matplotlib*** Version 3.3.2  
***numpy*** Version 1.19.2  
***pandas*** Version 1.1.2  
***seaborn*** Version 0.11.0  
***statsmodels*** Version 0.12.1  

### R packages
***data.table*** Version 1.13.2  
***ggplot2*** Version 3.3.2  
***ggfortify*** Version 0.4.11  
***impute*** Version 1.54.0  
***Rtsne*** Version 0.15   



## GTEx pipeline scripts
In this project, the first steps of a pipeline used in the GTEx paper are used in this study.  
These scripts are used: 
>- ***filter_psi.R***  
    Script for calculating and filtering the PSI values from rMATS
> - ***impute-PSI.R***  
    Imputing the PSI values using KNN
> - ***tSNE.R***  
    Calculating tSNE data from PSI value
> - ***tSNE-plot.R***  
    Making plots from the tSNE data

## Own scripts
In the study's own code are the following scripts, in the order in which they are used:
>- ***submit_batches.py***  
    Script for submitting rMATS jobs in batches to the UMCG cluster, in this script, generate_rMats_jobs.py is used to create the shell scripts for executing rMATS
>- ***calculate_psi.R***  
    Used for calculating PSI value per sample per dataset and then merging them, creating PSI files for each splice type and each dataset.
>- ***merge_and_filter.R***  
    Merges files for each dataset per splice type and filters the PSI values.
>- ***impute-PSI.R***  
    Imputes PSI data using KNN
>- ***PCA.R***  
    Performs the sample quality control by removing outlier samples and creating PCA plots to visualize the effects of removing these outliers
>- ***logistic_regression.py***  
    Used logistic regression to correct the data for technical covariates.
>- ***covarOLS.py***  
    Identifies multicollinearity in the technical covariates data and removes these columns
>- ***PCA_correctedData.R***  
    Performs PCA on the residuals from the data correction
>- ***sQTL.py***  
    Uses logistic regression to find sQTLs and calculate their p-value based on the z-score of the model.
>- ***pvalCorrection.R***  
    Corrects the sQTL p-values using Benjamini-Hochberg
>- ***regrPlot.py***  
    Make regression plots for certain splice events